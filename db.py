import json
import time
from os.path import exists

class Db():
    last_id = 0
    todos = []
    filename = 'lyst-data.json'
    def __init__(self,filename=None):
        if(filename!=None):
            self.filename = filename
        if(exists(self.filename)):
            with open(self.filename,'r') as f:
                self.todos = json.loads(f.read())
                self.last_id = max(self.todos, key=lambda x:x['id'])["id"]+1

    def get_index(self,id):
        return next((i for i, item in enumerate(self.todos) if int(item["id"]) == int(id)), None)

    def commit(self):
        with open(self.filename, 'w') as f:
            json.dump(self.todos, f)
            self.last_id+=1

    def list(self):
        return self.todos

    def add(self,title):
        new = {
            'id':self.last_id,
            'title': title,
            'done':False,
            'timestamp':int(round(time.time() * 1000))
        }
        self.todos.append(new)
        self.commit()

    def edit(self,id,title):
        index = self.get_index(id)
        if(index == None):
            print("Errore: l'elemento non esiste")
        else:
            self.todos[index]["title"] = title
            self.commit()

    def delete(self,id):
        index = self.get_index(id)
        if(index == None):
            print("Errore: l'elemento non esiste")
        else:
            self.todos.pop(index)
            self.commit()

    def toggle(self,id):
        index = self.get_index(id)
        if(index == None):
            print("Errore: l'elemento non esiste")
        else:
            self.todos[index]["done"]= not self.todos[index]["done"]
            self.commit()

    def search(self,string):
        return list(filter(lambda todo: string.lower() in todo['title'].lower() , self.todos))
