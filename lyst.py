from db import Db

class Lyst():
    db = Db()
    #h >>> mostra tutti le possibili action
    def h(self):
        ret="h >>> mostra tutti le possibili action"
        ret+="\nls >>> mostra tutti i todos ordinati per data di inserimento decrescente"
        ret+="\na (params: title) >>> aggiunge un todo"
        ret+="\ne (params: id, title) >>> edita un todo"
        ret+="\nd (params: id) >>> cancella un todo"
        ret+="\nt (params: id) >>> fa il toggle del todo (done: true VS done: false)"
        ret+="\ns (params: il termine da cercare) >>> cerca tra i todos e ritorna i todos contenenti il termine ricercato nel titolo"
        return ret

    #ls >>> mostra tutti i todos ordinati per data di inserimento decrescente
    def ls(self,args):
        return sorted(self.db.list(), key=lambda d: d['timestamp'], reverse=True)

    #a (params: title) >>> aggiunge un todo
    def a(self,args):
        if(len(args)>1):
            return("errore, troppi argomenti")
        else:
            if(len(args[0])<5):
                return("errore, titolo troppo breve")
            else:
                self.db.add(args[0])
        pass

    #e (params: id, title) >>> edita un todo
    def e(self,args):
        if(len(args)!=2):
            return("il numero di argomenti è errato, ne aspetto 2")
        else:
            if(len(args[1])<5):
                return("errore, titolo troppo breve")
            else:
                try:
                    int(args[0])
                    self.db.edit(args[0],args[1])
                except ValueError:
                    return("errore, indice non numerico")
        pass

    #d (params: id) >>> cancella un todo
    def d(self,args):
        if(len(args)!=1):
            return("il numero di argomenti è errato, ne aspetto 1")
        else:
            try:
                int(args[0])
                self.db.delete(args[0])
            except ValueError:
                return("errore, indice non numerico")
        pass

    #t (params: id) >>> fa il toggle del todo (done: true VS done: false)
    def t(self,args):
        if(len(args)!=1):
            return("il numero di argomenti è errato, ne aspetto 1")
        else:
            try:
                int(args[0])
                self.db.toggle(args[0])
            except ValueError:
                return("errore, indice non numerico")
        pass

    #s (params: il termine da cercare) >>> cerca tra i todos e ritorna i todos contenenti il termine ricercato nel titolo
    def s(self,args):
        if(len(args)!=1):
            return("il numero di argomenti è errato, ne aspetto 1")
        else:
            return(self.db.search(args[0]))


    def actions(self):
        acts = [func for func in dir(Lyst) if callable(getattr(Lyst, func)) and not func.startswith("__")]
        acts.remove("actions")
        return acts
