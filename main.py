import sys
from lyst import Lyst

l = Lyst()

if __name__ == "__main__":
    if(len(sys.argv)>1):
        if(sys.argv[1] in l.actions()):
            res= getattr(l, sys.argv[1])(sys.argv[2:])
            if(res is not None):
                print(res)
        else:
            print(f"Error: {sys.argv[1]} non è un argomento valido")
            print("\"lyst h\" per vedere questa guida \n")
            print(l.h())
    else:
        print("Usage: lyst  \n")
        print(l.h())
