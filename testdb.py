import unittest
from db import Db
import os

class TestDb(unittest.TestCase):
    filename = "lyst_data_test.json"

    def test_init_file(self):
        if(os.path.exists(self.filename)):
            os.remove(self.filename)
        db = Db(self.filename)
        self.assertTrue([] == db.list())

    def test_last_id(self):
        if(os.path.exists(self.filename)):
            os.remove(self.filename)
        db = Db(self.filename)
        self.assertTrue(db.last_id==0)
        db.add("test")
        self.assertTrue(db.last_id==1)
        db.add("test")
        self.assertTrue(db.last_id==2)
        pass

    def test_add(self):
        if(os.path.exists(self.filename)):
            os.remove(self.filename)
        db = Db(self.filename)
        length = len(db.list())
        db.add("test")
        self.assertTrue(length == len(db.list())-1)
        pass

    def test_delete(self):        
        pass

    def test_edit(self):
        pass

if __name__ == '__main__':
    unittest.main()
